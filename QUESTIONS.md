# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

Answer: 2 1
Reason: 2 will be console logged before the timeout executes

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

Reason: A recursive function, the function will be called until the variable `d` is equal to 10, at this point the first condition will be false so the function will be allowed to print 10... now that function is complete the call stack is able to complete the previous functions outputing all the numbers down to the originial argument which was 0.

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

Issue: If you provide `0` (or any falsy value) to foo it would be redefined as 5 which may not be expected behaviour.
Fix: A good fix would be to use the nullish coalescing operator (??) as this would still allow 0 whilst reassigning `undefined` and `null` to 5.

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

Answer: 3
Reason: The function returned from foo is assigned to `bar` so when `bar` is called it is in the scope of the initial 1 that has been passed to it.

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

How to call: You would call the function with a variable (presumably a number) as the first parameter and a completion function as the second parameter which accepts the result of the synchronous operation.
eg:

```js
double(2, (result) => console.log(result)); // 4
```
