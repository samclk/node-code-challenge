# MedicSpot Tech Test

## Server

Node server has a `/locations` endpoint that validates the query string has atleast 2 characters before performing a SQL query on the database. Also sends appropriate error messages for:

- Database connection error
- No matching locations found

## Client

Client built using react whilst also utilising tailwindcss (`https://tailwindcss.com/`) for rapid prototyping. The `useEffect` listens for changes to the searchterm and then validates the query string is the correct length before making the API call. If an error is returned it is stored and displayed appropriately. Should successful locations be returned they are displayed with their respective longitude and latitude.

## Future improvements

1. Move `/locations` endpoint into it's own locations router.
2. Construct a frontend application instead of all code living in script tags.

### Usage instructions

Start server with:

```js
node index.js
```

Visit client:

```js
http://localhost:3000
```

Run API tests:

```js
npm test
```
