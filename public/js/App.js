const e = React.createElement;
const { useState, useEffect } = React;

// Location component
const Location = ({ name, latitude, longitude }) => {
  return (
    <li className="py-2 px-4 border-b border-gray-300 text-xs text-gray-800 flex justify-between items-center">
      <div>
        <span className="font-semibold">Name:</span>&nbsp;{name}&nbsp;
      </div>
      <div>
        <p>
          <span className="font-semibold">Latitude:</span>
          &nbsp;{latitude}
        </p>
        <p>
          <span className="font-semibold">Longitude:</span>
          &nbsp;{longitude}
        </p>
      </div>
    </li>
  );
};

// App component
const App = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [results, setResults] = useState([]);
  const [error, setError] = useState();

  useEffect(() => {
    // Client side validation to check length of string
    if (searchTerm.length <= 2) {
      setResults([]);
      setError();
      return;
    }

    const fetchData = async () => {
      const result = await fetch(
        `http://localhost:3000/locations?q=${searchTerm}`,
        { method: "GET" }
      );

      const data = await result.json();

      if (data.message) {
        setResults([]);
        setError(data.message);
        return;
      }
      setError();
      setResults(data);
    };

    fetchData();
  }, [searchTerm]);

  return (
    <div className="max-w-xl mx-auto my-6">
      <form>
        <input
          value={searchTerm}
          className="py-2 px-4 border border-gray-300 w-full"
          placeholder="Search for a location"
          onChange={(e) => setSearchTerm(e.target.value)}
        />
      </form>
      {error && (
        <div className="bg-red-200 rounded py-2 px-4">
          <p className="text-gray-800 text-xs">{error}</p>
        </div>
      )}
      {results.length > 0 && (
        <ul className="border border-b-0 border-gray-300">
          {results.map((result, i) => {
            return <Location key={i} {...result} />;
          })}
        </ul>
      )}
    </div>
  );
};

const domContainer = document.querySelector("#root");
ReactDOM.render(e(App), domContainer);
