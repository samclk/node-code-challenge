// Server code goes here
const express = require("express");
const path = require("path");
const app = express();
const sqlite3 = require("sqlite3");
const DB_PATH = "./data/locations.db";

(async () => {
  const db = new sqlite3.Database(DB_PATH);

  // Allow else to access static files in the public directory
  app.use(express.static("public"));

  // Serve index.html at '/'
  app.get("/", (req, res) => {
    res.sendFile(path.resolve(__dirname, "public", "index.html"));
  });

  app.get("/locations", async (req, res) => {
    // Remove quotes from query
    const queryString = req.query.q.replace(/[^a-zA-Z\s]/g, "");

    // Server side validation to check query string is longer than 2 characters
    if (queryString.length <= 2) return res.send([]);

    // Possibly cache this query
    const sql = `SELECT name, latitude, longitude FROM locations WHERE name LIKE '${queryString}%'`;
    db.all(sql, [], (err, rows) => {
      // Check for database error
      if (err) {
        db.close();
        res.status(500);
        const message = "Database connection error";
        return res.send({ message });
      }

      // If no rows are found return a message
      if (rows.length === 0) {
        const message = `There are no locations which match ${queryString}`;
        return res.send({ message });
      }

      // Sort rows so smallest matching name is first
      const sortedRows = rows.sort((a, b) => a.name.length - b.name.length);
      res.send(sortedRows);
    });
  });
})();

module.exports = app;
