const app = require("./server");
const supertest = require("supertest");

test("GET /locations with a correct search term", async () => {
  await supertest(app)
    .get("/locations?q=Princethorpe")
    .expect(200)
    .then((response) => {
      // Check data
      expect(response.body[0].name).toBe("Princethorpe");
      expect(response.body[0].latitude).toBe("52.33342");
      expect(response.body[0].longitude).toBe("-1.41955");
    });
});

test("GET /locations with a search term that is less than 2 characters long", async () => {
  await supertest(app)
    .get("/locations?q=Pr")
    .expect(200)
    .then((response) => {
      expect(response.body.length).toBe(0);
    });
});

test("GET /locations with a search term that has no location matches", async () => {
  await supertest(app)
    .get("/locations?q=incorrect")
    .expect(200)
    .then((response) => {
      expect(response.body.message).toBe(
        "There are no locations which match incorrect"
      );
    });
});
